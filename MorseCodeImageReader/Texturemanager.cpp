#include "TextureManager.h"

#include <stdio.h>

#include <string>

TextureManager::TextureManager()
{
	// call this ONLY when linking with FreeImage as a static library
	#ifdef FREEIMAGE_LIB
		FreeImage_Initialise();
	#endif
}

TextureManager::~TextureManager()
{
	// call this ONLY when linking with FreeImage as a static library
	#ifdef FREEIMAGE_LIB
		FreeImage_DeInitialise();
	#endif
}

std::string TextureManager::LoadTexture(const char* filename)
{
	//image format
	FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;
	//pointer to the image, once loaded
	FIBITMAP *dib(0);
	//pointer to the image data
	BYTE* bits(0);
	//image width and height
	unsigned int width(0), height(0);
	
	//check the file signature and deduce its format
	fif = FreeImage_GetFileType(filename, 0);
	//if still unknown, try to guess the file format from the file extension
	if(fif == FIF_UNKNOWN) 
		fif = FreeImage_GetFIFFromFilename(filename);
	//if still unkown, return failure
	if(fif == FIF_UNKNOWN)
		return false;

	//check that the plugin has reading capabilities and load the file
	if(FreeImage_FIFSupportsReading(fif))
		dib = FreeImage_Load(fif, filename);
	//if the image failed to load, return failure
	/*if(!dib)
		return false;*/

	bits = FreeImage_GetBits(dib);

	width = FreeImage_GetWidth(dib);
	height = FreeImage_GetHeight(dib);

	/*if((bits == 0) || (width == 0) || (height == 0))
		return false;*/

	//FREE_IMAGE_COLOR_TYPE t = FreeImage_GetColorType(dib);
	//FREE_IMAGE_COLOR_TYPE::

	FIBITMAP* bitsmaps = FreeImage_ConvertTo32Bits(dib);
	
	int count = 0;

	std::string sReturn;

	for (int h = height - 1; h >= 0; --h)
	{
		for (int w = 0; w < width; ++w)
		{
			RGBQUAD* k = new RGBQUAD();

			bool b = FreeImage_GetPixelColor(bitsmaps, w, h, k);
			

			if (k->rgbBlue == (BYTE)255)
			{
				sReturn += (char)count;

				printf("%c", (char)count);
				count = 0;
			}
			count++;
		}
	}

	//Free FreeImage's copy of the data
	FreeImage_Unload(dib);

	//return success
	return sReturn;
}