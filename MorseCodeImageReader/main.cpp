
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <string>

#include <map>

using namespace std;


#include "TextureManager.h"

int main()
{
	ifstream inFile;
	ofstream outFile;

	inFile.open("C:\\HTS\\Programming 2\\PNG.png", ios::binary);
	outFile.open("C:\\HTS\\Programming 2\\answer.txt");

	int iError = 0;

	if (inFile.is_open() == false)
	{
		printf("image not found\n");
		iError = -1;
	}
	
	if (outFile.is_open() == false)
	{
		printf("answer document not found\n");
		iError = -1;
	}

	inFile.close();

	if (iError != 0)
	{
		outFile.close();
		return iError;
	}

	TextureManager tm;
	std::string s = tm.LoadTexture("C:\\HTS\\Programming 2\\PNG.png");
	
	std::map<std::string, std::string> codes;
    codes["A"] = ".-";
    codes["B"] = "-...";
    codes["C"] = "-.-.";
    codes["D"] = "-..";
    codes["E"] = ".";
    codes["F"] = "..-.";
    codes["G"] = "--.";
    codes["H"] = "....";
    codes["I"] = "..";
    codes["J"] = ".---";
    codes["K"] = "-.-";
    codes["L"] = ".-..";
    codes["M"] = "--";
    codes["N"] = "-.";
    codes["O"] = "---";
    codes["P"] = ".--.";
    codes["Q"] = "--.-";
    codes["R"] = ".-.";
    codes["S"] = "...";
    codes["T"] = "-";
    codes["U"] = "..-";
    codes["V"] = "...-";
    codes["W"] = ".--";
    codes["X"] = "-..-";
    codes["Y"] = "-.--";
    codes["Z"] = "--..";
    codes["1"] = ".----";
    codes["2"] = "..---";
    codes["3"] = "...--";
    codes["4"] = "....-";
    codes["5"] = ".....";
    codes["6"] = "-....";
    codes["7"] = "--...";
    codes["8"] = "---..";
    codes["9"] = "----.";
    codes["0"] = "-----";

	std::string temp;

	printf("\n\n");

	std::string answer;

	for (int i = 0; i < s.length(); ++i)
	{
		if (s[i] == ' ')
		{
			for(auto iter = codes.begin(); iter != codes.end(); ++iter)
			{
				if (iter->second == temp)
				{
					printf("%s", iter->first.c_str());
					answer += iter->first.c_str();
				}
			}

			temp.clear();
			continue;
		}

		temp += s[i];
	}

	//MessageBox(0, answer.c_str(), "WHATEVER", MB_OK);

	HGLOBAL hText;
	char *pText;
	hText = GlobalAlloc(GMEM_DDESHARE|GMEM_MOVEABLE, 100);
	pText = (char*)GlobalLock(hText);
	strcpy(pText, answer.c_str());
	GlobalUnlock(hText);

	OpenClipboard(NULL);
	EmptyClipboard();
	SetClipboardData(CF_TEXT, hText);
	CloseClipboard();

	return 0;
}