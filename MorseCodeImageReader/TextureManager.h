//**********************************************
//Singleton Texture Manager class
//Written by Ben English
//benjamin.english@oit.edu
//
//For use with OpenGL and the FreeImage library
//**********************************************

#ifndef TextureManager_H
#define TextureManager_H

#include <windows.h>
#include <gl/gl.h>
#include "FreeImage.h"
#include <map>

#include <string>

class TextureManager
{
public:
	TextureManager();

	virtual ~TextureManager();

	std::string LoadTexture(const char* filename);
};

#endif